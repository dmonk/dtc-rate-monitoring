FROM cern/cc7-base

RUN yum install -y python-pip
RUN curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.centos7.x86_64.repo -o /etc/yum.repos.d/ipbus-sw.repo
RUN yum groupinstall -y uhal

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

ENV LD_LIBRARY_PATH=/opt/cactus/lib:$LD_LIBRARY_PATH
ENV PATH=/opt/cactus/bin:$PATH

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt

WORKDIR /app
COPY . /app

CMD ["python", "script.py"]
