import uhal


def readAddress(device, address, safe=True):
    data = 0
    try:
        data = device.getNode(address).read()
        device.dispatch()
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            pass
    finally:
        return data


def writeAddress(device, address, value, safe=True):
    data = 0
    try:
        device.getNode(address).write(value)
        device.dispatch()
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            pass
    finally:
        return data    


def readMemory(manager, address, width, safe=True):
    data = 0
    try:
        data = manager.getNode(address).readBlock(int(2**width))
        manager.dispatch()
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            pass
    finally:
        return data


def readChannelAddress(device, channel, address, safe=True):
    data = 0
    try:
        device.getNode(channel[0]).write(channel[1])
        data = device.getNode(address[0]).read()
        device.dispatch()
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            pass
    finally:
        return data


def readChannelMemory(device, channel, address, width, safe=True):
    data = 0
    try:
        device.getNode(channel[0]).write(channel[1])
        data = device.getNode(address[0]).readBlock(int(2**width))
        device.dispatch()
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            pass
    finally:
        return data