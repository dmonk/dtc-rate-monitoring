from prometheus_client import start_http_server, Gauge, Counter, Histogram
import uhal
import random
import time
import os
from ipbtools import readAddress, readMemory, writeAddress, readChannelAddress, readChannelMemory
import pandas as pd
import numpy as np
from copy import deepcopy
from func import setSingleAddress, setFEChannelAddress, setFECICChannelAddress, FEChannelCounter, MProcessorChannelCounter, ChannelHistogram

DEVICE="x1"
package = os.getenv("PACKAGE")
CBC_MAPPING = [
    [7, 6, 5, 4, 0, 1, 2, 3],
    [0, 1, 2, 3, 7, 6, 5, 4]  
]


strips = Counter('module_strips', 'Hit count of each strip in module', ['strip', 'cic'])
for i in range(int(2**9)):
    strips.labels(strip=i, cic=0)
    strips.labels(strip=i, cic=1)
binned_strips = Counter('binned_module_strips', 'Binned hit counts', ['bucket', 'cic'])
for i in range(8):
    binned_strips.labels(bucket=i, cic=0)
    binned_strips.labels(bucket=i, cic=1)
overflow_status = Gauge('link_aggregator_overflow_status', 'Status bits for the link aggregator', ['fifo', 'index'])
for i in range(8):
    overflow_status.labels(fifo="output", index=i)
lff_status = Gauge('lff_status', 'Status bit for the lff signal from 10G ethernet block')

la_nonempty = Counter('link_aggregator_non_empty', 'Count of instances where the cache FIFO is reset before it can be emptied', ['bx', 'link'])
for i in range(8):
    for j in range(6):
        la_nonempty.labels(bx=i, link=j)

run_number = Gauge('run_number', 'Run number as defined in firmware')

stub_count = Counter('stub_count', 'Number of stubs recorded', ['link', 'cic'])
for i in range(4):
    for j in range(2):
        stub_count.labels(link=i, cic=j)
header_misalignment_count = Counter('header_misalignment_count', 'Number of header misalignments recorded', ['link', 'cic'])
for i in range(4):
    for j in range(2):
        header_misalignment_count.labels(link=i, cic=j)
header_reset_count = Counter('header_reset_count', 'Number of header resets recorded', ['link', 'cic'])
for i in range(4):
    for j in range(2):
        header_reset_count.labels(link=i, cic=j)
aligner_state = Gauge('aligner_state', 'Status of header aligner', ['link', 'cic'])
for i in range(4):
    for j in range(2):
        aligner_state.labels(link=i, cic=j)
uplink_rdy = Gauge('uplink_rdy', 'Status of uplink ready signal', ['link'])
for i in range(4):
        uplink_rdy.labels(link=i)
stub_count_hist = Gauge('stub_count_hist', 'Histogram of packet stub count', ['link', 'cic', 'count'])
for i in range(4):
    for j in range(2):
        for k in range(int(2**6)):
            stub_count_hist.labels(link=i, cic=j, count=k)

if __name__ == '__main__':
    uhal.disableLogging()
    # Start up the server to expose the metrics.
    start_http_server(8000)
    print(package)
    print("file://{}/connections.xml".format(package))
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    hw = manager.getDevice(DEVICE)
    prev_count0 = 0
    prev_count1 = 0
    count0 = 0
    count1 = 0

    non_empty_counts = MProcessorChannelCounter(
        la_nonempty,
        "payload.mprocessor.link_aggregator.monitoring.non_empty_count_"
    )
    stub_counts = FEChannelCounter(
        stub_count,
        "payload.fe_chan.health_mon.status.stub_count"
    )
    header_misalignment_counts = FEChannelCounter(
        header_misalignment_count,
        "payload.fe_chan.health_mon.status.header_mismatch_count"
    )
    header_reset_counts = FEChannelCounter(
        header_reset_count,
        "payload.fe_chan.health_mon.status.header_reset_count"
    )
    stub_counts_hist = ChannelHistogram(
        stub_count_hist, 
        "payload.fe_chan.health_mon.status.stub_count_hist_max",
        "payload.fe_chan.health_mon.stub_count_mem",
        6
    )

    while True:
        # FE Histogram
        try:
            count0 = readAddress(hw, "payload.csr.histogram0")
            count1 = readAddress(hw, "payload.csr.histogram1")
            #print(count0, count1)
            if count0 < prev_count0 or count1 < prev_count1:
                df = pd.DataFrame()
                df["CIC_0"] = readMemory(hw, "payload.mem1", 9)
                df["CIC_1"] = readMemory(hw, "payload.mem2", 9)
                # print(df)
                for i in range(int(2**9)):
                    strip = (CBC_MAPPING[0][i >> 6] << 6) + 64 - (i%64)
                    strips.labels(strip=strip, cic=0).inc(df["CIC_0"][i])
                    strip = (CBC_MAPPING[1][i >> 6] << 6) + (i%64)
                    strips.labels(strip=strip, cic=1).inc(df["CIC_1"][i])

                for i in range(int(2**9)):
                    bucket = CBC_MAPPING[0][i >> 6]
                    binned_strips.labels(bucket=bucket, cic=0).inc(df["CIC_0"][i])
                    bucket = CBC_MAPPING[1][i >> 6]
                    binned_strips.labels(bucket=bucket, cic=1).inc(df["CIC_1"][i])
                #    for _ in range(df["CIC_0"][i]):
                #        strip_histogram.labels(cic=0).observe(val)
                #for i in range(int(2**9)):
                #    val = 2*CBC_MAPPING[1][i >> 6] + i % 2
                #    for _ in range(df["CIC_1"][i]):
                #        strip_histogram.labels(cic=1).observe(val)
            prev_count0 = count0
            prev_count1 = count1

        except uhal._core.exception as e:
            pass

        # Link Aggregator non-empty count
        non_empty_counts.updateValues(hw)

        # Run Number
        setSingleAddress(run_number, hw, 'eth10g.channel_0.ctrl.reg.run')

        # FE stub counts
        stub_counts.updateValues(hw)

        # FE header misalignments
        header_misalignment_counts.updateValues(hw)

        # FE header resets
        header_reset_counts.updateValues(hw)

        # FE aligner state
        setFECICChannelAddress(aligner_state, hw, "payload.fe_chan.health_mon.status.aligner_status")

        # FE uplink rdy
        setFEChannelAddress(uplink_rdy, hw, "payload.fe_chan.health_mon.status.uplink_rdy")

        # FE Stub count histogram
        stub_counts_hist.updateValues(hw)

        time.sleep(1)
