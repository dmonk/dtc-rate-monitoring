import uhal
import numpy as np
from constants import FE_COUNT, CIC_COUNT
from ipbtools import readAddress, readChannelAddress, readChannelMemory
from copy import deepcopy


def setSingleAddress(metric, hw, address):
    try:
        value = int(readAddress(hw, address))
        metric.set(value)
    except uhal._core.exception as e:
        pass


def setFECICChannelAddress(metric, hw, address):
    try:
        for i in range(FE_COUNT):
            for j in range(CIC_COUNT):
                metric.labels(link=i, cic=j).set(int(readChannelAddress(
                    hw, 
                    ("payload.fe.chan_sel", i), 
                    ("{}{}".format(address, j),)
                )))
    except uhal._core.exception as e:
        pass


def setFEChannelAddress(metric, hw, address):
    try:
        for i in range(FE_COUNT):
            metric.labels(link=i).set(int(readChannelAddress(
                hw, 
                ("payload.fe.chan_sel", i), 
                ("{}".format(address),)
            )))
    except uhal._core.exception as e:
        pass


class ChannelCounter:
    def __init__(self, metric, address):
        self.metric = metric
        self.address = address
        self.counts = np.zeros((self.ranges[0], self.ranges[1]), dtype=int)
        self.counts_prev = np.zeros((self.ranges[0], self.ranges[1]), dtype=int)

    def updateValues(self, hw):
        try:
            for i in range(self.ranges[0]):
                for j in range(self.ranges[1]):
                    self.counts[i][j] = int(readChannelAddress(
                        hw, 
                        (self.channel_address, i), 
                        ("{}{}".format(self.address, j),)
                    ))
            if np.any(self.counts >= self.counts_prev):
                diff = self.counts - self.counts_prev
            else:
                diff = self.counts
            for i in range(self.ranges[0]):
                for j in range(self.ranges[1]):
                    self.incMetric(i, j, diff[i][j])
            self.counts_prev = deepcopy(self.counts)
        except uhal._core.exception as e:
            pass


class FEChannelCounter(ChannelCounter):
    channel_address = "payload.fe.chan_sel"
    ranges = (FE_COUNT, CIC_COUNT)

    def incMetric(self, i, j, value):
        self.metric.labels(link=i, cic=j).inc(value)


class MProcessorChannelCounter(ChannelCounter):
    channel_address = "payload.mprocessor.link_aggregator.chan_sel"
    ranges = (8, 6)

    def incMetric(self, i, j, value):
        self.metric.labels(bx=i, link=j).inc(value)


class ChannelHistogram:
    def __init__(self, metric, count_address, mem_address, mem_width):
        self.metric = metric
        self.count_address = count_address
        self.mem_address = mem_address
        self.mem_width = mem_width
        self.max_count = np.zeros((FE_COUNT, CIC_COUNT), dtype=int)
        self.max_count_prev = np.zeros((FE_COUNT, CIC_COUNT), dtype=int)
    
    def updateValues(self, hw):
        try:
            for i in range(FE_COUNT):
                for j in range(CIC_COUNT):
                    self.max_count[i][j] = int(readChannelAddress(
                        hw, 
                        ("payload.fe.chan_sel", i), 
                        ("{}{}".format(self.count_address, j),)
                    ))
            if np.any(self.max_count < self.max_count_prev):
                for i in range(FE_COUNT):
                    for j in range(CIC_COUNT):
                        data = [int(k) for k in readChannelMemory(
                            hw, 
                            ("payload.fe.chan_sel", i), 
                            ("{}{}".format(self.mem_address, j),),
                            width=self.mem_width
                        )]
                        for k in range(int(2**self.mem_width)):
                            self.metric.labels(link=i, cic=j, count=k).set(int(data[k]))
            self.max_count_prev = deepcopy(self.max_count)
        except uhal._core.exception as e:
            pass