# DTC Rate Monitoring

Docker container to export rate statistics to a Prometheus server.

## Usage

```
/opt/cactus/bin/serenity/docker-run.sh -d --name dtcratemon -p 8000:8000 -v $PACKAGE:/package -e PACKAGE=/package gitlab-registry.cern.ch/dmonk/dtc-rate-monitoring/dtcratemon:master-100d61a0
```
Where `$PACKAGE` is the path of the currently running package
